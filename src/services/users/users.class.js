const { Service } = require('feathers-mongodb');

const crypto = require('crypto');

// Chceck for avatar on gravatar
const gravatarUrl = 'https://s.gravatar.com/avatar';
// with size of 60
const query = 's=60';

exports.Users = class Users extends Service {
  constructor(options, app) {
    super(options);

    app.get('mongoClient').then(db => {
      this.Model = db.collection('users');
    });
  }
  // Overwrite create method
  create(data, params) {
    const { email, login, password, githubId } = data;

    const hash = crypto.createHash('md5').update(email.toLowerCase()).digest('hex');

    const avatar = `${gravatarUrl}/${hash}?${query}`;

    const created = new Date();

    const userData = {
      email,
      login,
      password,
      githubId,
      avatar,
      admin: false,
      created
    };

    return super.create(userData, params);
  }

  // patch(id, data, params) {
  //   id = params.user._id;
  //   return super.patch(id, data, params);
  // }

  // remove(id, params) {
  //   id = params.user._id;
  //   return super.remove(id, params);
  // }
};
