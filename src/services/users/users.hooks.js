const { authenticate } = require('@feathersjs/authentication').hooks;

const {
  hashPassword, protect
} = require('@feathersjs/authentication-local').hooks;

const validateUser = require('../../hooks/validate-user');

const secureUsers = require('../../hooks/secure-users');

const preventRequest = require('../../hooks/prevent-request');

// const queryAdapter = require('../../hooks/query-adapter');

module.exports = {
  before: {
    all: [],
    find: [authenticate('jwt'), secureUsers(), ],
    get: [authenticate('jwt'), secureUsers()],
    create: [hashPassword('password'), validateUser()],
    update: [ hashPassword('password'),  authenticate('jwt'), preventRequest() ],
    patch: [ hashPassword('password'),  authenticate('jwt') ],
    remove: [ authenticate('jwt') ]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
