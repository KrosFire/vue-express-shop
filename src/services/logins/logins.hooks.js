

const preventRequest = require('../../hooks/prevent-request');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [preventRequest()],
    update: [preventRequest()],
    patch: [preventRequest()],
    remove: [preventRequest()]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
