// Initializes the `logins` service on path `/logins`
const { Logins } = require('./logins.class');
const hooks = require('./logins.hooks');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/api/logins', new Logins(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('api/logins');

  service.hooks(hooks);
};
