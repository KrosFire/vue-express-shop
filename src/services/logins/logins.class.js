const { Service } = require('feathers-mongodb');

exports.Logins = class Logins extends Service {
  constructor(options, app) {
    super(options);

    app.get('mongoClient').then(db => {
      this.Model = db.collection('users');
    });
  }
  find(params) {
    params.query = {
      $select: ['login', 'email']
    };
    return super.find(params);
  }
  get(params) {
    params.query = {
      $select: ['login']
    };
    return super.get(params);
  }
};
