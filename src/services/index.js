const users = require('./users/users.service.js');
const logins = require('./logins/logins.service.js');
const products = require('./products/products.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(logins);
  app.configure(products);
};
