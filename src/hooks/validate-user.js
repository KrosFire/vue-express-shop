// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    const { data } = context;

    console.log('create method');
    console.log(data);

    // checks if user has all the data
    if (!data.login ) {
      // checks if user logs with github
      if((!data.password || !data.email) && !data.githubId){
        throw new Error('Email, login and password are required.');
      // eslint-disable-next-line no-extra-boolean-cast
      }
    }

    if(!data.email) {
      data.email = '';
    }
    if(!data.password) {
      data.password = '';
    }

    return context;
  };
};
