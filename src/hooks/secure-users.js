// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {

    // Checks if a user has privilege to access data about the registered users
    if (!!context.params.authenticated && !!context.params.user) {
      if(context.params.user.admin){
        // if it's an admin give access to all the users
        context.params.query = {};

      } else {
        context.params.query = {
          $limit: 1,
          login: context.params.user.login
        };
      }
    }
    return context;
  };
};
