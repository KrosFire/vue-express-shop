const csruf = require('csurf');

const csrfProtection = csruf({
  cookie: true
});

// const getCsrf = app => {
//   app.use('/getcsrf', csrfProtection, (req, res) => {
//     return res.json({ csrfToken: req.csrfToken() });
//   })
// }

module.exports = {
  csrfProtection,
  getCsrf: app => {
    app.use('/api/getcsrf', csrfProtection, (req, res) => {
      return res.json({
        csrfToken: req.csrfToken()
      });
    });
  }
};
