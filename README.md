# Usage
Run `npm install` in root folder.

Change the name of '.env.sample' file to '.env'.

Next run `npm run dev` to start backend.

Open new terminal window. Go to /client and run `npm install` and `npm run serve` to start frontend.

# Project description

Backend server runs on Feathersjs and connects to mongodb database. Frontend (temporarily) runs on Vuejs and connects with backend through sockets and feathersvuex.
In the future frontend will run on nuxtjs.

Design will be changed according to vuetify standards.