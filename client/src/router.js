import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Register from './views/Register.vue';
import Signin from './views/Signin.vue';
import UserPanel from './views/UserPanel.vue';
import NotFound from './views/404.vue';
import store from './store';
import OAuth from './views/OAuth.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      component: NotFound,
    },
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      beforeEnter: (to, from, next) => {
        const loggedIn = store.getters['auth/isAuthenticated'];
        if (loggedIn) {
          next({ name: 'userPanel' });
        } else {
          next();
        }
      },
    },
    {
      path: '/signin',
      name: 'signin',
      component: Signin,
      beforeEnter: (to, from, next) => {
        const loggedIn = store.getters['auth/isAuthenticated'];
        if (loggedIn) {
          next({ name: 'userPanel' });
        } else {
          next();
        }
      },
    },
    {
      path: '/panel',
      name: 'userPanel',
      component: UserPanel,
      beforeEnter: (to, from, next) => {
        const loggedIn = store.getters['auth/isAuthenticated'];
        if (!loggedIn) {
          next({ name: 'signin' });
        } else {
          next();
        }
      },

    },
    {
      path: '/oauth',
      name: 'oauth',
      component: OAuth,
    },
  ],
});
