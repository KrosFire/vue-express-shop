import Vue from 'vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VueCookies from 'vue-cookies';
import App from './App.vue';
import router from './router';
import store from './store';


library.add(fas);
library.add(fab);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(VueCookies);

Vue.config.productionTip = false;

async function start() {
  // Auto authenticate
  const token = localStorage.getItem('feathers-jwt');
  if (token) {
    await store.dispatch('auth/authenticate');
  }

  new Vue({
    store,
    router,
    render: h => h(App),
  }).$mount('#app');
}

start();
