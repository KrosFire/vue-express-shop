import safe from 'safe-regex';

class TestInput {
  static testLogin(login) {
    const log = login.trim();
    const data = {
      empty: false,
      pattern: false,
    };

    if (log === '') data.empty = true;

    const pattern = /^(?![_ -])(?:(?![_ -]{2})[\w -]){5,12}(?<![_ -])$/;

    if (safe(log)) {
      if (!pattern.test(log)) {
        data.pattern = true;
      }
    }

    return data;
  }

  static confirmPass(password) {
    const pass = password.trim();
    const data = {
      empty: false,
      pattern: false,
    };
    if (pass === '') data.empty = true;

    const pattern = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@$!%*#?&]{8,16}$/;
    if (safe(pass)) {
      if (!pattern.test(pass)) {
        data.pattern = true;
      }
    }

    return data;
  }

  static testEmail(email) {
    const mail = email.trim();
    const data = {
      empty: false,
      pattern: false,
    };

    if (mail === '') data.empty = true;

    // eslint-disable-next-line no-control-regex
    const pattern = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    if (safe(mail)) {
      if (!pattern.test(mail)) {
        data.pattern = true;
      }
    }

    return data;
  }
}

export default TestInput;
