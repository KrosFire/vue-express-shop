const ToPanel = {
  beforeCreate() {
    if (this.$cookies.isKey('login')) {
      this.$router.push({ path: '/userPanel' });
    }
  },
};

const ToLogin = {
  beforeCreate() {
    if (!this.$cookies.isKey('login')) {
      this.$router.push('signin');
    }
  },
};

export { ToPanel, ToLogin };
