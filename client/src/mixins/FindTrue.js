/* eslint-disable no-restricted-syntax */
const FindTrue = (...arr) => {
  for (const tab of arr) {
    for (const el of tab) {
      if (el === true) return true;
    }
  }
  return false;
};

export default FindTrue;
