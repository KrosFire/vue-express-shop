import { setField } from 'feathers-authentication-hooks';
import feathersClient, { makeServicePlugin, BaseModel } from '../../feathers-client';

class User extends BaseModel {
  // eslint-disable-next-line no-useless-constructor
  constructor(data, options) {
    super(data, options);
  }

  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = 'api/users'

  // Define default properties here
  static instanceDefaults() {
    return {
      user: null,
    };
  }
}
const servicePath = 'api/users';
const servicePlugin = makeServicePlugin({
  Model: User,
  service: feathersClient.service(servicePath),
  servicePath,
  idField: '_id',
  nameStyle: 'path',
});

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [
      setField({
        from: 'params.user._id',
        as: 'params.query._id',
      }),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
});

export default servicePlugin;
