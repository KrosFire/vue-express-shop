import axios from 'axios';

export default {
  namespaced: true,
  state: {
    csrf: '',
  },

  mutations: {
    ADD_CSRF(state, csrf) {
      state.csrf = csrf;
    },
  },

  actions: {
    async getCsrf({ commit }) {
      const { data } = await axios.get('/api/getcsrf');

      commit('ADD_CSRF', data.csrfToken);
    },
  },
};
