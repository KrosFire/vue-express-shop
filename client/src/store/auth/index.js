import { makeAuthPlugin } from '../../feathers-client';

export default makeAuthPlugin({
  userService: 'api/users',
  entityIdField: '_id',
  debug: true,
});
