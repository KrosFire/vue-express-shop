import Cookies from 'vue-cookies';
import store from './store';

const updateFromCookies = () => {
  const loggedIn = Cookies.isKey('login');
  if (loggedIn) {
    console.log('adding user');
    store.dispatch('getUserData');
  } else {
    console.log('removing user');
    store.commit('rmUser');
  }
};

export default updateFromCookies;
