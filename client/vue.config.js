module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:3030',
      },
    },
  },

  transpileDependencies: ['feathers-vuex'],

  outputDir: '../public',
  lintOnSave: undefined,
};
