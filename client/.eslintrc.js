module.exports = {
  root: true,

  env: {
    node: true,
  },

  extends: [
    'plugin:vue/essential',
    '@vue/prettier',
  ],

  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    'vue/html-indent': 'error',
    'vue/html-quotes': 'error'
  },

  parserOptions: {
    parser: 'babel-eslint',
  },

  'extends': [
    'plugin:vue/recommended',
    '@vue/airbnb'
  ]
};
